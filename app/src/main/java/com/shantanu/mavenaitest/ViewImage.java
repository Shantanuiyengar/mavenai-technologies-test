package com.shantanu.mavenaitest;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.zolad.zoominimageview.ZoomInImageView;

import java.io.File;

public class ViewImage extends AppCompatActivity {
    String filename;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_image);
        filename = getIntent().getStringExtra("file");
        File file = new File(getExternalCacheDir(),filename);
        ZoomInImageView image = findViewById(R.id.image);
        image.setImageURI(Uri.fromFile(file));
        findViewById(R.id.back).setOnClickListener(v -> onBackPressed());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this,DetectorActivity.class));
        overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
        finish();
    }
}