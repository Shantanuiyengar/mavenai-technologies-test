package com.shantanu.mavenaitest;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

public class MainActivity extends AppCompatActivity {
    private GoogleSignInClient mGoogleSignInClient;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getSharedPreferences(getPackageName()+"270997",0).getBoolean("logged_in",false) && checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED){
            startActivity(new Intent(this,DetectorActivity.class));
            overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
            finish();
        }
        else if(checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED){
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},27);
        }
        else{
            setContentView(R.layout.activity_main);
            Glide.with(this).asGif().load(R.drawable.login).into((ImageView) findViewById(R.id.illustration));
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();
            mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
            if(checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)
                findViewById(R.id.sign_in).setOnClickListener(v -> {
                    Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                    startActivityForResult(signInIntent, 27);
                });
            else
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},27);
        }
    }

    boolean isPermissionAskedAgain = false;
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == 27){
            if(getSharedPreferences(getPackageName()+"270997",0).getBoolean("logged_in",false) && checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED){
                startActivity(new Intent(this,DetectorActivity.class));
                overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                finish();
            }
            else if(!getSharedPreferences(getPackageName()+"270997",0).getBoolean("logged_in",false)){
                findViewById(R.id.sign_in).setOnClickListener(v -> {
                    Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                    startActivityForResult(signInIntent, 27);
                });
            }
            else{
                if(!isPermissionAskedAgain){
                    isPermissionAskedAgain = true;
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},27);
                }
                // If asked twice and still denied, move user to Settings page to manually enable permissions
                else{
                    Toast.makeText(this,"Permissions Denied. Please enable permissions from Settings",Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    intent.setData(Uri.parse("package:" + getPackageName()));
                    startActivity(intent);
                    finish();
                }
            }
        }
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == 27) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                assert account != null;
                updateUI(account);
            } catch (ApiException e) {
                Log.w("TAG", "signInResult:failed code=" + e.getStatusCode());
                Toast.makeText(this, "Sign In Failed..\nPlease try again", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void updateUI(GoogleSignInAccount account) {
        getSharedPreferences(getPackageName()+"270997",0).edit().putBoolean("logged_in",true).apply();
        getSharedPreferences(getPackageName()+"270997",0).edit().putString("email",account.getEmail()).apply();
        getSharedPreferences(getPackageName()+"270997",0).edit().putString("name",account.getDisplayName()).apply();
        getSharedPreferences(getPackageName()+"270997",0).edit().putString("image", String.valueOf(account.getPhotoUrl())).apply();
        startActivity(new Intent(this,DetectorActivity.class));
        overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
        finish();
    }
}